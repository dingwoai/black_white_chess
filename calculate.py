from game.reversi import Reversi

def find_the_best(map, chess_color = 1):
    r = Reversi(map, chess_color)
    return r.pick_move()
